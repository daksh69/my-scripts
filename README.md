# Personal Scripts

| Script Name  | Description                                                                           |
| ------       | ------                                                                                |
| checkup      | Script by Luke Smith for checking and updating the system , can be used as a cron job |
| compiler     | Script by Luke Smith for compiling C , C++ , Rust etc programmes automatically        |
| dmenuunicode | Script by Luke Smith for displaying and copying emojis , require libxft-bgra          |
| ext          | Script by Luke Smith for extracing common archive formats                             |
| fe           | Script for quick editing files in current dir using $EDITOR                           |
| flame        | Script for using flameshot with Print                                                 |
| obup         | Script for updating openbox menu using menumaker                                      |
| pipe         | Script for starting pipewire , pipewire-pulse and pipewire-media-session              |
| qadd         | Script inspired by Luke Smith's transadd script , adds torrent in qbittorrent         |
| setbg        | Script by Luke Smith , without pywal integration                                      |
| ytf          | Script for using ytfzf + extra options                                                |
